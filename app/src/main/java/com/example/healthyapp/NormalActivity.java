package com.example.healthyapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

public class NormalActivity extends AppCompatActivity {

    TextView textViewNormal;
    Button buttonFinish;
    float imc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normal);

        Objects.requireNonNull(getSupportActionBar()).hide();

        textViewNormal = findViewById(R.id.text_imc_normal);
        buttonFinish = findViewById(R.id.button_finish);

        imc = getIntent().getFloatExtra("IMC", 0);

        Resources res = getResources();

        textViewNormal.setText(String.format(res.getString(R.string.imc), imc));

        buttonFinish.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(NormalActivity.this);
            builder.setTitle(R.string.dialog_title_low);
            builder.setCancelable(true);
            builder.setMessage(R.string.back_to_main_msg);
            builder.setPositiveButton("Si", (dialog, which) -> {
                Intent intent = new Intent(NormalActivity.this, MainActivity.class);
                startActivity(intent);
            });
            builder.setNegativeButton("No", (dialog, which) -> {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            });
            builder.create().show();
        });
    }
}