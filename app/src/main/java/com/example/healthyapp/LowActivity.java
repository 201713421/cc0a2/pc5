package com.example.healthyapp;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

public class LowActivity extends AppCompatActivity {

    TextView textViewImc, textViewDesc;
    Spinner spinnerOptions;
    ImageView imageViewRecommendantions;
    Button buttonTerminate;
    float imc;

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_low);

        Objects.requireNonNull(getSupportActionBar()).hide();

        textViewImc = findViewById(R.id.text_imc_low);
        spinnerOptions = findViewById(R.id.spinner_options);
        imageViewRecommendantions = findViewById(R.id.image_low_options);
        textViewDesc = findViewById(R.id.subtitle);
        buttonTerminate = findViewById(R.id.button_finish);

        imc = getIntent().getFloatExtra("IMC", 0);

        Resources res = getResources();

        textViewImc.setText(String.format(res.getString(R.string.imc), imc));
        adapter = ArrayAdapter.createFromResource(this, R.array.optionslow, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOptions.setAdapter(adapter);

        spinnerOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    imageViewRecommendantions.setImageResource(R.drawable.foodincrease);
                    textViewDesc.setText(R.string.increase_food);
                }
                if (position == 1) {
                    imageViewRecommendantions.setImageResource(R.drawable.licuados);
                    textViewDesc.setText(R.string.nutrient_drinks);
                }
                if (position == 2) {
                    imageViewRecommendantions.setImageResource(R.drawable.nutrientes);
                    textViewDesc.setText(R.string.nutrient_food);
                }
                if (position == 3) {
                    imageViewRecommendantions.setImageResource(R.drawable.agua);
                    textViewDesc.setText(R.string.less_drinks);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonTerminate.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(LowActivity.this);
            builder.setTitle(R.string.dialog_title_low);
            builder.setCancelable(true);
            builder.setMessage(R.string.back_to_main_msg);
            builder.setPositiveButton("Si", (dialog, which) -> {
                Intent intent = new Intent(LowActivity.this, MainActivity.class);
                startActivity(intent);
            });
            builder.setNegativeButton("No", (dialog, which) -> {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            });
            builder.create().show();
        });
    }
}