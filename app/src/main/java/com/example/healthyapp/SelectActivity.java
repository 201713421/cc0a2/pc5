package com.example.healthyapp;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Objects;

public class SelectActivity extends AppCompatActivity {

    TextView textViewIMC, textViewStatus;
    Button buttonFinish;
    GridView gridViewExercises;
    float imc;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();
    int flex = 30, zanc = 60, sent = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        Objects.requireNonNull(getSupportActionBar()).hide();

        gridViewExercises = findViewById(R.id.grid_view_exercise);
        fillArray();

        textViewIMC = findViewById(R.id.text_imc_high);
        textViewStatus = findViewById(R.id.text_status_high);
        buttonFinish = findViewById(R.id.button_finish);

        imc = getIntent().getFloatExtra("IMC", 0);

        Resources res = getResources();

        if (imc >= 25 && imc <= 29.9) {
            textViewIMC.setText(String.format(res.getString(R.string.imc), imc));
            textViewStatus.setText(R.string.sobrepeso);
            textViewIMC.setTextColor(ContextCompat.getColor(this, R.color.text_col));
            textViewStatus.setTextColor(ContextCompat.getColor(this, R.color.text_col));
        }

        else {
            textViewIMC.setText(String.format(res.getString(R.string.imc), imc));
            textViewStatus.setText(R.string.obesidad);
            textViewIMC.setTextColor(ContextCompat.getColor(this, R.color.red));
            textViewStatus.setTextColor(ContextCompat.getColor(this, R.color.red));
        }

        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridViewExercises.setAdapter(gridAdapter);

        gridViewExercises.setOnItemClickListener((parent, view, position, id) -> {
            String exercise = text.get(position);
            AlertDialog.Builder builder = new AlertDialog.Builder(SelectActivity.this);
            builder.setTitle(exercise);
            builder.setCancelable(true);
            if (exercise.equals(res.getString(R.string.correr))){
                builder.setMessage(R.string.correr_msg);
            }
            else if (exercise.equals(res.getString(R.string.planchas))) {
                builder.setMessage(String.format(res.getString(R.string.exercise_msg), flex, exercise));
            }
            else if (exercise.equals(res.getString(R.string.zancadas))) {
                builder.setMessage(String.format(res.getString(R.string.exercise_msg), zanc, exercise));
            }
            else if (exercise.equals(res.getString(R.string.sentadilla))) {
                builder.setMessage(String.format(res.getString(R.string.exercise_msg), sent, exercise));
            }
            builder.create().show();
        });

        buttonFinish.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(SelectActivity.this);
            builder.setTitle(R.string.dialog_title_low);
            builder.setCancelable(true);
            builder.setMessage(R.string.back_to_main_msg);
            builder.setPositiveButton("Si", (dialog, which) -> {
                Intent intent = new Intent(SelectActivity.this, MainActivity.class);
                startActivity(intent);
            });
            builder.setNegativeButton("No", (dialog, which) -> {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            });
            builder.create().show();
        });
    }

    private void fillArray(){
        text.add(getResources().getString(R.string.correr));
        text.add(getResources().getString(R.string.planchas));
        text.add(getResources().getString(R.string.zancadas));
        text.add(getResources().getString(R.string.sentadilla));

        image.add(R.drawable.correr);
        image.add(R.drawable.flexiones);
        image.add(R.drawable.zancadas);
        image.add(R.drawable.sentadillas);
    }
}