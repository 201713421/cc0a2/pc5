package com.example.healthyapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    EditText editTextHeight, editTextWeight;
    Button buttonConfirm;
    CheckBox checkBoxAge;
    SharedPreferences sharedPreferences;
    float imc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Objects.requireNonNull(getSupportActionBar()).hide();

        editTextHeight = findViewById(R.id.edit_altura);
        editTextWeight = findViewById(R.id.edit_peso);
        buttonConfirm = findViewById(R.id.button_confirm);
        checkBoxAge = findViewById(R.id.check_box_age);

        buttonConfirm.setOnClickListener(v -> {
            String sHeight = editTextHeight.getText().toString();
            String sWeight = editTextWeight.getText().toString();

            if (sHeight.equals("") || sWeight.equals("")){
                Toast.makeText(MainActivity.this, R.string.toast_fill, Toast.LENGTH_LONG).show();
            }

            else{
                if (checkBoxAge.isChecked()){
                    float dHeight = Float.parseFloat(sHeight);
                    int iWeight = Integer.parseInt(sWeight);

                    Resources res = getResources();

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(false);
                    builder.setMessage(String.format(res.getString(R.string.confirmation_data_msg), dHeight, iWeight));
                    builder.setPositiveButton("Si", (dialog, which) -> {
                        saveData();
                        imc = CalculateIMC(dHeight, iWeight);
                        if (imc < 18.5) {
                            Intent intent = new Intent(MainActivity.this, LowActivity.class);
                            intent.putExtra("IMC", imc);
                            startActivity(intent);
                        }
                        else if (imc >= 18.5 && imc <= 24.9) {
                            Intent intent = new Intent(MainActivity.this, NormalActivity.class);
                            intent.putExtra("IMC", imc);
                            startActivity(intent);
                        }
                        else {
                            Intent intent = new Intent(MainActivity.this, SelectActivity.class);
                            intent.putExtra("IMC", imc);
                            startActivity(intent);
                        }
                    });
                    builder.setNegativeButton("No", (dialog, which) -> {
                    });
                    builder.create().show();
                }
                else {
                    Toast.makeText(MainActivity.this, R.string.toast_age, Toast.LENGTH_LONG).show();
                }
            }
        });

        retrieveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    protected float CalculateIMC(float height, int weight) {
        float imc;
        imc = (float) (weight) / (height * height);
        return imc;
    }

    private void saveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        String height = editTextHeight.getText().toString();
        String weight = editTextWeight.getText().toString();
        boolean isChecked = checkBoxAge.isChecked();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key height", height);
        editor.putString("key weight", weight);
        editor.putBoolean("key remember", isChecked);
        editor.apply();
    }

    private void retrieveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        String height = sharedPreferences.getString("key height", null);
        String weight = sharedPreferences.getString("key weight", null);
        boolean isChecked = sharedPreferences.getBoolean("key remember", false);

        editTextHeight.setText(height);
        editTextWeight.setText(weight);
        checkBoxAge.setChecked(isChecked);
    }
}